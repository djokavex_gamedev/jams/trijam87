extends Sprite

var bullet = preload("res://scene/Bullet.tscn")

var timer_shot = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	look_at(get_global_mouse_position())
	rotation_degrees = rotation_degrees - 90
	
	var time_now = OS.get_ticks_msec()
	var elapse = (time_now - timer_shot) / 1000.0
	
	if Input.is_action_just_pressed("ui_accept") and (elapse > 2):
		timer_shot = OS.get_ticks_msec()
		get_parent().get_node("Camera2D").shake_amount = 0.2
		randomize()
		var shot_vel = Vector2(1000, rand_range(-200,200))
		var shot_pos = Vector2(60, 0)
		shot_vel = shot_vel.rotated(get_parent().rotation + rotation + 1.57)
		shot_pos = shot_pos.rotated(get_parent().rotation + rotation + 1.57)
		
		var my_bullet = bullet.instance()
		my_bullet.position = global_position
		my_bullet.position = my_bullet.position + shot_pos
		my_bullet.rotation_degrees = get_parent().rotation_degrees + rotation_degrees + 90
		my_bullet.velocity = shot_vel
		get_parent().get_node("ShotScene").add_child(my_bullet)
		pass
		
	#print(str(elapse) + " " + str(elapse / 2.0 * 100.0))
	get_parent().get_node("PlayerGui/ShotReady").value = elapse / 2.0 * 100.0
	
	pass
