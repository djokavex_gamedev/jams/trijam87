extends Sprite

var bullet = preload("res://scene/Bullet.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	look_at(get_parent().get_parent().get_node("Player").position)
	rotation_degrees = rotation_degrees - 90
	
	# Only shot if not drifting
#	if Input.is_action_just_pressed("ui_accept") and (not get_parent().get_parent().get_node("Player").is_drifting):
#		randomize()
#		var shot_vel = Vector2(1000, rand_range(-50,50))
#		var shot_pos = Vector2(60, 0)
#		shot_vel = shot_vel.rotated(get_parent().rotation + rotation + 1.57)
#		shot_pos = shot_pos.rotated(get_parent().rotation + rotation + 1.57)
#
#		var my_bullet = bullet.instance()
#		my_bullet.position = global_position
#		my_bullet.position = my_bullet.position + shot_pos
#		my_bullet.rotation_degrees = get_parent().rotation_degrees + rotation_degrees + 90
#		my_bullet.velocity = shot_vel
#		get_parent().get_node("ShotScene").add_child(my_bullet)
#		pass
		
	pass


func _on_ShotTimer2_timeout():
	if not get_parent().get_parent().get_node("Player").is_drifting:
		randomize()
		var shot_vel = Vector2(1000, rand_range(-50,50))
		var shot_pos = Vector2(60, 0)
		shot_vel = shot_vel.rotated(get_parent().rotation + rotation + 1.57)
		shot_pos = shot_pos.rotated(get_parent().rotation + rotation + 1.57)

		var my_bullet = bullet.instance()
		my_bullet.position = global_position
		my_bullet.position = my_bullet.position + shot_pos
		my_bullet.rotation_degrees = get_parent().rotation_degrees + rotation_degrees + 90
		my_bullet.velocity = shot_vel
		get_parent().get_node("ShotScene").add_child(my_bullet)
		pass
	pass # Replace with function body.
