extends KinematicBody2D

var MAX_SPEED = 100
var MAX_ROTATION = 2

var motion = Vector2(0,0)
var rotation_speed = 0

var health = 20 setget set_health
var is_drifting = true
var last_drifting = OS.get_ticks_msec()

signal dead

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("player")
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var vel = Vector2(0,0)
	
	if is_drifting:
		if Input.is_action_pressed("ui_right"):
			rotation_speed = rotation_speed + 5 * delta
		if Input.is_action_pressed("ui_left"):
			rotation_speed = rotation_speed - 5 * delta
			
		#vel.x = int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left"))
		vel.x = int(Input.is_action_pressed("ui_down")) - int(Input.is_action_pressed("ui_up"))
			
		var current_motion = vel.normalized() * 10
		
		if rotation_speed > MAX_ROTATION:
			rotation_speed = MAX_ROTATION
		if rotation_speed < -MAX_ROTATION:
			rotation_speed = -MAX_ROTATION
		
		if current_motion.x > MAX_SPEED:
			current_motion.x = MAX_SPEED
		if current_motion.x < -MAX_SPEED:
			current_motion.x = -MAX_SPEED
			
		if current_motion.y > MAX_SPEED:
			current_motion.y = MAX_SPEED
		if current_motion.y < -MAX_SPEED:
			current_motion.y = -MAX_SPEED
			
		rotation_degrees = rotation_degrees + rotation_speed
		
		current_motion = current_motion.rotated(rotation)
		if current_motion.x > MAX_SPEED:
			current_motion.x = MAX_SPEED
		if current_motion.x < -MAX_SPEED:
			current_motion.x = -MAX_SPEED
			
		if current_motion.y > MAX_SPEED:
			current_motion.y = MAX_SPEED
		if current_motion.y < -MAX_SPEED:
			current_motion.y = -MAX_SPEED
			
		motion = motion + current_motion
		if motion.x > MAX_SPEED:
			motion.x = MAX_SPEED
		if motion.x < -MAX_SPEED:
			motion.x = -MAX_SPEED
			
		if motion.y > MAX_SPEED:
			motion.y = MAX_SPEED
		if motion.y < -MAX_SPEED:
			motion.y = -MAX_SPEED
		
		pass
	else:
		rotation_speed = 0
		if Input.is_action_pressed("ui_right"):
			rotation_speed = rotation_speed + 50 * delta
		if Input.is_action_pressed("ui_left"):
			rotation_speed = rotation_speed - 50 * delta
		
		vel = Vector2(0,0)
		#vel.x = int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left"))
		vel.x = int(Input.is_action_pressed("ui_down")) - int(Input.is_action_pressed("ui_up"))
			
		var current_motion = vel.normalized() * 100
		
		if rotation_speed > MAX_ROTATION:
			rotation_speed = MAX_ROTATION
		if rotation_speed < -MAX_ROTATION:
			rotation_speed = -MAX_ROTATION
		
		if current_motion.x > MAX_SPEED:
			current_motion.x = MAX_SPEED
		if current_motion.x < -MAX_SPEED:
			current_motion.x = -MAX_SPEED
			
		if current_motion.y > MAX_SPEED:
			current_motion.y = MAX_SPEED
		if current_motion.y < -MAX_SPEED:
			current_motion.y = -MAX_SPEED
			
		rotation_degrees = rotation_degrees + rotation_speed
		
		current_motion = current_motion.rotated(rotation)
		if current_motion.x > MAX_SPEED:
			current_motion.x = MAX_SPEED
		if current_motion.x < -MAX_SPEED:
			current_motion.x = -MAX_SPEED
			
		if current_motion.y > MAX_SPEED:
			current_motion.y = MAX_SPEED
		if current_motion.y < -MAX_SPEED:
			current_motion.y = -MAX_SPEED
			
		motion = current_motion
		if motion.x > MAX_SPEED:
			motion.x = MAX_SPEED
		if motion.x < -MAX_SPEED:
			motion.x = -MAX_SPEED
			
		if motion.y > MAX_SPEED:
			motion.y = MAX_SPEED
		if motion.y < -MAX_SPEED:
			motion.y = -MAX_SPEED
		pass
	
		
	
	
	move_and_slide(motion)
	
	$PlayerGui/Life.value = health
	
	var time_now = OS.get_ticks_msec()
	var elapse = (time_now - last_drifting) / 1000.0
	if elapse > 0.2:
		is_drifting = true
		
	if not is_drifting:
		if $crosshair.visible == false:
			$crosshair.visible = true
			$crosshair.scale = Vector2(1.5, 1.5)
			$Tween.interpolate_property($crosshair, "scale", Vector2(1.5, 1.5), Vector2(1.0, 1.0), 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN)
			$Tween.start()
	else:
		$crosshair.visible = false
	
	pass

func set_health(value):
	if value > 20:
		value = 20

	health = value
	$Tween.interpolate_property($Sprite, "modulate", Color(1, 1, 1, 1), Color(1, 1, 1, 0), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.interpolate_property($Sprite, "modulate", Color(1, 1, 1, 0), Color(1, 1, 1, 1), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN, 0.05)
	$Tween.start()

		
	if health <= 0:
		emit_signal('dead')
		#print("test2")
		#emit_signal('dead', elapse)
		#get_tree().change_scene("res://scene/LoginScreen.tscn")
	pass
