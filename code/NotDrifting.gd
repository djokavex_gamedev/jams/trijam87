extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var overlapping_areas = get_overlapping_bodies()
	for area in overlapping_areas:
		if area.is_in_group("player"):
			area.is_drifting = false
			area.last_drifting = OS.get_ticks_msec()
		#area.queue_free()
	pass
