extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	$Player.connect("dead", self, "player_dead")
	$Enemy.connect("win", self, "player_win")
	$Enemy2.connect("win", self, "player_win")
	$BigEnemy.connect("win", self, "player_win")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func player_dead():
	var music = AudioStreamPlayer.new()
	self.add_child(music)
	music.stream = load('res://snd/game_over.ogg')
	music.play()
	$CanvasLayer/Pause/Label.text = "Game over!"
	$CanvasLayer/Pause.set_pause()
	pass
	
func player_win():
	var music = AudioStreamPlayer.new()
	self.add_child(music)
	music.stream = load('res://snd/you_win.ogg')
	music.play()
	$CanvasLayer/Pause/Label.text = "Congratulation!"
	$CanvasLayer/Pause.set_pause()
	pass
