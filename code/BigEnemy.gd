extends Area2D

var health = 10 setget set_health

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("enemy")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func set_health(value):
	if value > 10:
		value = 10

	health = value
	$Tween.interpolate_property($Sprite, "modulate", Color(1, 1, 1, 1), Color(1, 1, 1, 0), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.interpolate_property($Sprite, "modulate", Color(1, 1, 1, 0), Color(1, 1, 1, 1), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN, 0.05)
	$Tween.start()

		
	if health <= 0:
		var music = AudioStreamPlayer.new()
		self.add_child(music)
		music.stream = load('res://snd/war_target_destroyed.ogg')
		music.play()
		globals.current_enemies = globals.current_enemies - 1
		if globals.current_enemies <= 0:
			emit_signal('win')
		queue_free()
		#print("test2")
		#emit_signal('dead', elapse)
		#get_tree().change_scene("res://scene/LoginScreen.tscn")
	pass
