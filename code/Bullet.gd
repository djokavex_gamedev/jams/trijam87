extends Area2D

var velocity = Vector2(0,0)

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("weapon")
	set_process(true)
	connect("area_entered", self, "_on_area_enter")
	connect("body_entered", self, "_on_body_enter")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	global_translate(velocity * delta)
	pass

func _on_area_enter(other):
	if other == get_parent().get_parent():
		return
	if other.is_in_group("player") and get_parent().get_parent().is_in_group("enemy"):
		other.health = other.health - 1
		velocity = Vector2(0,0)
		var music = AudioStreamPlayer.new()
		self.add_child(music)
		music.stream = load('res://snd/Explosion_03.wav')
		music.play()
		$AnimationPlayer.play("explosion")
		yield($AnimationPlayer,"animation_finished")
		queue_free()
	if other.is_in_group("enemy") and get_parent().get_parent().is_in_group("player"):
		other.health = other.health - 1
		velocity = Vector2(0,0)
		var music = AudioStreamPlayer.new()
		self.add_child(music)
		music.stream = load('res://snd/Explosion_03.wav')
		music.play()
		$AnimationPlayer.play("explosion")
		yield($AnimationPlayer,"animation_finished")
		queue_free()
	pass
	
func _on_body_enter(other):
	if other == get_parent().get_parent():
		return
	if other.is_in_group("player") and get_parent().get_parent().is_in_group("enemy"):
		other.health = other.health - 1
		velocity = Vector2(0,0)
		var music = AudioStreamPlayer.new()
		self.add_child(music)
		music.stream = load('res://snd/Explosion_03.wav')
		music.play()
		$AnimationPlayer.play("explosion")
		yield($AnimationPlayer,"animation_finished")
		queue_free()
	if other.is_in_group("enemy") and get_parent().get_parent().is_in_group("player"):
		other.health = other.health - 1
		velocity = Vector2(0,0)
		var music = AudioStreamPlayer.new()
		self.add_child(music)
		music.stream = load('res://snd/Explosion_03.wav')
		music.play()
		$AnimationPlayer.play("explosion")
		yield($AnimationPlayer,"animation_finished")
		queue_free()
	pass
