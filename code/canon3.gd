extends Sprite

var bullet = preload("res://scene/Bullet.tscn")
var time = 1.5

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	look_at(get_parent().get_parent().get_node("Player").position)
	rotation_degrees = rotation_degrees - 90
	
#	if Input.is_action_just_pressed("ui_accept"):
#		randomize()
#		var shot_vel = Vector2(1000, rand_range(-300,300))
#		if not get_parent().get_parent().get_node("Player").is_drifting:
#			shot_vel = Vector2(1000, rand_range(-50,50))
#		var shot_pos = Vector2(60, 0)
#		shot_vel = shot_vel.rotated(get_parent().rotation + rotation + 1.57)
#		shot_pos = shot_pos.rotated(get_parent().rotation + rotation + 1.57)
#
#		var my_bullet = bullet.instance()
#		my_bullet.position = global_position
#		my_bullet.position = my_bullet.position + shot_pos
#		my_bullet.rotation_degrees = get_parent().rotation_degrees + rotation_degrees + 90
#		my_bullet.velocity = shot_vel
#		get_parent().get_node("ShotScene").add_child(my_bullet)
#		pass
		
	pass



func _on_ShotTimer_timeout():
	randomize()
	var shot_vel = Vector2(1000, rand_range(-300,300))
	if not get_parent().get_parent().get_node("Player").is_drifting:
		shot_vel = Vector2(1000, rand_range(-50,50))
	var shot_pos = Vector2(60, 0)
	shot_vel = shot_vel.rotated(get_parent().rotation + rotation + 1.57)
	shot_pos = shot_pos.rotated(get_parent().rotation + rotation + 1.57)
	
	var my_bullet = bullet.instance()
	my_bullet.position = global_position
	my_bullet.position = my_bullet.position + shot_pos
	my_bullet.rotation_degrees = get_parent().rotation_degrees + rotation_degrees + 90
	my_bullet.velocity = shot_vel
	get_parent().get_node("ShotScene").add_child(my_bullet)
	
	get_parent().get_node("ShotTimer").wait_time = time + rand_range(-0.2, 0.2)

	pass # Replace with function body.
